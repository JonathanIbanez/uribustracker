package edu.uri.uribustracker;

/**
 * Created by ray on 4/17/16.
 *
 * The Bus class holds all of the data for the buses along the routes.
 * This class generates the location, vehicle number, position, and
 * the route of each bus.
 *
 * @author emilyhendricks
 */
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.maps.model.LatLng;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.HashSet;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Bus {
    public LatLng realPos;
    public Date lastUpdate;
    public Route.DynamicInterpolationResult lastInterpolatedPos;
    public Route.DynamicInterpolationResult interpolatedPos;
    public boolean pendingRedraw = false;
    public Route mostLikelyRoute() {
        if (redRouteWeight < 4*blueRouteWeight) {
            return Route.getHillClimberRoute();
        } else {
            return Route.getBlueRoute();
        }
    }
    private double redRouteWeight = 0;
    private double blueRouteWeight = 0;
    private int updates = 0;
    private ConcurrentLinkedQueue<Route.InterpolationResult> last5;
    public void setInterpolatedPos(Route.DynamicInterpolationResult pos) {
        if (pos.route == Route.getHillClimberRoute()) {
            redRouteWeight += pos.result.distance;
            blueRouteWeight += Route.getBlueRoute().closestPoint(pos.result.point).distance;
        } else {
            blueRouteWeight += pos.result.distance;
            redRouteWeight += Route.getHillClimberRoute().closestPoint(pos.result.point).distance;
        }
        updates++;
        if (last5 == null) last5 = new ConcurrentLinkedQueue<Route.InterpolationResult>();
        try {
            if (lastInterpolatedPos != null && pos.result.point.latitude != lastInterpolatedPos.result.point.latitude) {
                last5.add(pos.result);
                if (last5.size() > 5) {
                    last5.poll();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        lastInterpolatedPos = interpolatedPos;
        interpolatedPos = pos;
    }
    /**
     * Returns a boolean representing whether or not the bus looks like it's not actually following a path or is on break. Currently
     * this takes too much computing power to do efficiently.
     * @return a boolean whether or not the bus is probably not moving
     */
    public boolean isSuspicious() {
        if (true) return false;
        HashSet<Integer> segments = new HashSet<Integer>();
        boolean lastSign = false;
        int i = 0, lastPath = 0, signChanges = 0;
        if (last5 == null) last5 = new ConcurrentLinkedQueue<Route.InterpolationResult>();
        for (Iterator<Route.InterpolationResult> it = last5.iterator(); it.hasNext(); ++i) {
            Route.InterpolationResult result = it.next();
            boolean sign = lastPath > result.pathSegment;
            if (i > 1) {
                if (lastSign && sign) {}
                else { signChanges++; }
            }
            if (i > 0) lastSign = sign;
            lastPath = result.pathSegment;
            segments.add(result.pathSegment);
        }
        if (segments.size() < 3 && last5.size() > 3) {
            return true;
        }
        if (signChanges > 2 && last5.size() > 3) {
            return true;
        }
        return false;
    }
    public String time;
    public String route;
    public String vehicle;
    public Marker marker;
    public MarkerOptions markerOpts;
}
