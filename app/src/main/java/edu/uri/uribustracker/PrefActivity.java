package edu.uri.uribustracker;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.view.View;
import android.preference.Preference;


public class PrefActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
        PreferenceManager.setDefaultValues(getBaseContext(), R.xml.preferences, false);
    }
    public static class SettingsFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            Preference hillclimber = (Preference) findPreference(getString(R.string.red_toggle));
            hillclimber.getEditor().putBoolean(getString(R.string.red_toggle), RouteSelector.isSelected(Route.getHillClimberRoute())).commit();
            hillclimber.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference a) {
                    RouteSelector.toggle(Route.getHillClimberRoute());
                    return true;
                }
            });
            Preference blueroute = (Preference) findPreference(getString(R.string.blue_toggle));
            blueroute.getEditor().putBoolean(getString(R.string.blue_toggle), RouteSelector.isSelected(Route.getBlueRoute())).commit();
            blueroute.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference a) {
                    RouteSelector.toggle(Route.getBlueRoute());
                    return true;
                }
            });
        }
    }

}
