package edu.uri.uribustracker;

import org.junit.Test;

import static org.junit.Assert.*;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ray on 4/25/16.
 */
public class RouteSelectorTest {
    @Test
    public void testUnselect() throws Exception {
        RouteSelector.selection = RouteSelector.RED_ROUTE;
        assertEquals(RouteSelector.unselect(RouteSelector.RED_ROUTE), true);
        assertEquals(RouteSelector.isSelected(Route.getHillClimberRoute()), false);
        assertEquals(RouteSelector.select(RouteSelector.RED_ROUTE), true);
        assertEquals(RouteSelector.unselect(), true);
        assertEquals(RouteSelector.unselect(), false);
    }
    @Test
    public void testSelect() throws Exception {
        RouteSelector.unselect();
        assertEquals(RouteSelector.select(RouteSelector.RED_ROUTE), true);
        assertEquals(RouteSelector.isSelected(Route.getHillClimberRoute()), true);
    }
    @Test
    public void testToggle() throws Exception {
        RouteSelector.unselect();
        assertEquals(RouteSelector.toggle(Route.getHillClimberRoute()), false);
        assertEquals(RouteSelector.isSelected(Route.getHillClimberRoute()), true);
    }
}
